off stat;
function x,p;
symbol n;
*local l1=x^4*p-p*x^4;
*local l2=x*p^4-p^4*x;
local l3 =x^n*p-p*x^n;
.sort;
repeat;
*id x*p = p*x + i_;
id x^n*p=x^(n-1)*(p*x+i_);
id x^n?*p=x^(n-1)*(p*x+i_);
endrepeat;
print;
.end
